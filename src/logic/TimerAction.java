package logic;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JTextField;
import entity.Attention;

/**
 * 获取用户输入的值.
 * @author AL
 */
public class TimerAction {
	
	/**
	 * 格式化时间.
	 */
	private final static SimpleDateFormat SDF = new SimpleDateFormat("HHmm");
	
	private static String userMsg;// 闹钟启动时的提示文字
	
	public static void action(JFrame frame, JTextField hour, JTextField minute, JTextField msg) {
		// 获取用户输入，并判断
		userMsg = msg.getText().trim();
		userMsg = userMsg.equals("输入提示信息") ? null : userMsg;

		// 获取并判断小时
		Integer hourValue = getInt(hour, 23);
		if (hourValue == null) {
			warning(hour);
			return;
		}

		// 获取并判断分钟
		Integer minValue = getInt(minute, 59);
		if (minValue == null) {
			warning(minute);
			return;
		}

		// 字符串补位
		String userHour = replenish(hourValue);
		String userMinute = replenish(minValue);

		// 退出主界面
		frame.dispose();
		
		// 启动定时器
		timer(userHour.concat(userMinute));
	}
	
	/**
	 * 从文本域获取用户输入的非负整数，并判断是否在范围内.
	 * @param hour
	 * @param range 最大值（包含）
	 * @return
	 */
	private static Integer getInt(JTextField hour, int range) {
		Integer value = null;
		try {
			value = Integer.parseInt(hour.getText().trim());
			int temp = value.intValue();
			if (temp > range || temp < 0) {
				value = null;
			}
		} catch (NumberFormatException e) {
			value = null;
		}
		return value;
	}
	
	/**
	 * 将一个不足2位的整数补齐2位.
	 * @param id
	 * @return
	 */
	public static String replenish(int id) {
		String currentId = Integer.toString(id);

		// 补齐8位
		int length = currentId.length();
		int shortage = 2 - length;
		if (shortage > 0) {
			char[] zeros = new char[]{'0','0'};
			System.arraycopy(currentId.toCharArray(),0,zeros,shortage,length);
			currentId = new String(zeros);
		}
		
		return currentId;
	}
	
	/**
	 * 定时器，当用户在主界面设置完毕后，主界面退出，此方法在内存中驻留
	 * 直至指定时间，启动闹钟界面，此方法退出.
	 * @param alarm
	 */
	private static void timer(String alarm) {
		while (true) {
			// 若时间到，则初始化闹钟界面
			if (SDF.format(new Date()).equals(alarm)) {
				new Attention(userMsg == null ? "时间到！" : userMsg);
				return;
			}
			try {// 每30秒循环获取一次当前时间
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				continue;
			}
		}
	}

	/**
	 * 警告用户输入异常.
	 * @param field
	 */
	private static void warning(JTextField field) {
		field.setForeground(Color.RED);
		field.setText("输入有误");
	}
	
}