package view;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

/**
 * 鼠标悬停事件样式类，用于美化界面.
 * @author AL
 */
public class ButtonAction implements MouseListener {
	
	private JButton btn;
	
	public ButtonAction(JButton btn) {
		this.btn = btn;
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		btn.setBackground(new Color(184,207,229));//蓝色偏灰
	}
	@Override
	public void mouseExited(MouseEvent e) {
		btn.setBackground(null);//恢复按钮默认颜色
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}
	
	@Override
	public void mousePressed(MouseEvent e) {}
	
	@Override
	public void mouseReleased(MouseEvent e) {}
	
}