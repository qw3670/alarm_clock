package view;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTextField;

/**
 * 文本框鼠标监听器.
 * @author AL
 */
public class TextFieleAction implements MouseListener {

	private JTextField jtf;

	public TextFieleAction(JTextField jtf) {
		this.jtf = jtf;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 当用户单击文本输入框时，清空文字并设置前景色为黑色
		jtf.setText("");
		jtf.setForeground(Color.BLACK);
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}