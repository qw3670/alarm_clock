package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import logic.TimerAction;

/**
 * 简单闹钟程序.
 * @author AL
 */
public class Door extends JFrame {
	private static final long serialVersionUID = 1L;

	/**
	 * 公用字体.
	 */
	public static final Font FONT = new Font("Microsoft yahei", Font.BOLD, 20);
	
	private JTextField hour;// 小时输入框
	private JTextField minute;// 分钟输入框
	private JTextField input;// 闹钟提示信息输入框
	private JButton start;// 按钮
	private JPanel panel;

	public static void main(String[] args) {
		new Door();
	}

	/**
	 * 创建初始界面.
	 * @param title
	 */
	private Door() {
		super("闹钟");// 初始界面
		hour = new JTextField();// 小时输入框
		minute = new JTextField();// 分钟输入框
		input = new JTextField();// 闹钟提示信息输入框
		start = new JButton("完成");// 按钮
		panel = new JPanel();

		hour.setText("输入时(0~23)");
		minute.setText("输入分(0~59)");
		input.setText("输入提示信息");
		hour.setFont(FONT);
		minute.setFont(FONT);
		input.setFont(FONT);
		hour.setForeground(Color.GRAY);
		minute.setForeground(Color.GRAY);
		input.setForeground(Color.GRAY);
		start.setFont(FONT);

		Container cp = getContentPane();
		cp.setLayout(new GridLayout(3, 1));
		panel.setLayout(new GridLayout(1, 2));

		panel.add(hour);
		panel.add(minute);
		cp.add(panel);
		cp.add(input);
		cp.add(start);

		// 监听鼠标动作
		start.addMouseListener(new ButtonAction(start));// 鼠标悬停时，改变按钮样式
		hour.addMouseListener(new TextFieleAction(hour));// 当点击输入框时，文字清空，下同
		minute.addMouseListener(new TextFieleAction(minute));
		input.addMouseListener(new TextFieleAction(input));
		
		// 绑定鼠标点击事件
		start.addActionListener(e -> TimerAction.action(this, hour, minute, input));// 读取用户的输入值，用于启动定时器

		setSize(320, 200);
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}