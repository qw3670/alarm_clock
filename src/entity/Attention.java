package entity;

import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import static view.Door.FONT;

/**
 * 到点提醒界面.
 * @author Rex
 *
 */
public class Attention extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JTextArea msg;
	private JScrollPane scroll;
	
	public Attention(String words) {
		super("时间到！");
		msg = new JTextArea();
		scroll = new JScrollPane(msg);

		msg.setEditable(false);
		msg.setFont(FONT);
		msg.setText(words);
		// 设置文本框内文字自动换行（即：始终不需要横向滚动条）
		msg.setLineWrap(true);

		// 设置横向和纵向滚动条只在需要时出现
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		Container cp = getContentPane();
		cp.setLayout(new GridLayout(1, 1));
		cp.add(scroll);

		setSize(250, 100);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}